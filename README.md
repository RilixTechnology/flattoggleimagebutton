

 FlatToogleImageButton is a custom ImageButton of Android with "Flat UI" concept. This library is very small and highly customizable.

Screenshot
----------
![](screenshot/screenshot.gif)

Including in your project
-------------------------
### Using Maven

    dependencies {
        compile 'com.rilixtech.widget:flattoggleimagebutton:0.0.8'
    }
    

Customizable attributes
-----------------------

|  Attribute           |   default value   | xml                  |                 java                |
|----------------------|-------------------|----------------------|-------------------------------------|
| check state          |      false        | checked              | setChecked(boolean isChecked)       |
| button color         |      #3eadeb      | ftb_buttonColor      | setButtonColor(int color)           |
| enable shadow        |        true       | ftb_shadowEnabled    | setShadowEnabled(boolean isEnabled) |
| shadow color         |  Automatically generated <br> from button color   | flb_shadowColor   | setShadowColor(int color)           |
| shadow height        |        4dp        | ftb_shadowHeight     | setShadowHeight(int height)         |
| corner radius        |        8dp        | ftb_cornerRadius     | setCornerRadius(int radius)         |
| checked color        |     #3eadeb       | ftb_checkedColor     | setCheckedColor(int color)          |
| unchecked color      |     #ecf0f1       | ftb_uncheckedColor   | setUncheckedColor(int color)        |
| checked text color   |  Color.WHITE      | ftb_checkedTextColor | setCheckedTextColor(int color)      |
| unchecked text color |  Color.GRAY       | ftb_checkedTextColor | setUncheckedTextColor(int color)    |

Usage
-----
If the default values of custom attributes did not meet your requirement, you can easily re-config that attributes. This is sample code that you can refer. you can also browse demo app for more details.

### via xml (sample)

-  Define `xmlns:flb="http://schemas.android.com/apk/res-auto"` on root of your xml file

```xml
<com.rilixtech.widget.flattoggleimagebutton.FlatToggleImageButton
    ...
   fbutton:ftb_buttonColor="@color/color_concrete"
   fbutton:ftb_shadowColor="@color/color_asbestos"
   fbutton:ftb_shadowEnabled="true"
   fbutton:ftb_shadowHeight="5dp"
   fbutton:ftb_cornerRadius="5dp"
    ...
     />
```

### via code (sample)
```java
btnImage.setButtonColor(getResources().getColor(R.color.color_concrete));
btnImage.setShadowColor(getResources().getColor(R.color.color_asbestos));
btnImage.setShadowEnabled(true);
btnImage.setShadowHeight(5);
btnImage.setCornerRadius(5);
```

Color Swatches
--------------
For your convenience Swatches Preset by [designmono](http://designmodo.github.io/Flat-UI/) are also defined in this library
```xml
 //Color Swatches provided by http://designmodo.github.io/Flat-UI/
<color name="fbutton_color_turquoise">#1abc9c</color>
<color name="fbutton_color_green_sea">#16a085</color>
<color name="fbutton_color_emerald">#2ecc71</color>
<color name="fbutton_color_nephritis">#27ae60</color>
<color name="fbutton_color_peter_river">#3498db</color>
<color name="fbutton_color_belize_hole">#2980b9</color>
<color name="fbutton_color_amethyst">#9b59b6</color>
<color name="fbutton_color_wisteria">#8e44ad</color>
<color name="fbutton_color_wet_asphalt">#34495e</color>
<color name="fbutton_color_midnight_blue">#2c3e50</color>
<color name="fbutton_color_sun_flower">#f1c40f</color>
<color name="fbutton_color_orange">#f39c12</color>
<color name="fbutton_color_carrot">#e67e22</color>
<color name="fbutton_color_pumpkin">#d35400</color>
<color name="fbutton_color_alizarin">#e74c3c</color>
<color name="fbutton_color_pomegranate">#c0392b</color>
<color name="fbutton_color_clouds">#ecf0f1</color>
<color name="fbutton_color_silver">#bdc3c7</color>
<color name="fbutton_color_concrete">#95a5a6</color>
<color name="fbutton_color_asbestos">#7f8c8d</color>
```

Developed By
-------
Rilix Technology

License
-------
       Copyright 2019 Rilix Technology 
    
       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at
    
           http://www.apache.org/licenses/LICENSE-2.0
    
       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
       
