package com.rilixtech.widget.flattoggleimagebutton;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.concurrent.atomic.AtomicInteger;

public class FlatToggleImageButton extends LinearLayout implements Checkable, View.OnTouchListener {
  private static final String TAG = FlatToggleImageButton.class.getSimpleName();
  private OnCheckedChangeListener mListener;

  public interface OnCheckedChangeListener {
    void onCheckedChanged(FlatToggleImageButton button, boolean isChecked);
  }

  //Custom values
  private boolean mIsShadowEnabled = true;
  private int mButtonColor;
  private int mShadowColor;
  private int mShadowHeight;
  private int mCornerRadius;
  //Native values
  private int mPaddingLeft;
  private int mPaddingRight;
  private int mPaddingTop;
  private int mPaddingBottom;
  //Background drawable
  private Drawable mPressedDrawable;
  private Drawable mUnpressedDrawable;

  boolean mIsShadowColorDefined = false;

  private int mCheckedColor;
  private int mUncheckedColor;
  private int mCheckedTextColor;
  private int mUncheckTextColor;

  private ImageView mImvImage;
  private TextView mTvText;

  public FlatToggleImageButton(Context context) {
    this(context, null);
    setOnTouchListener(this);
  }

  public FlatToggleImageButton(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
    parseAttrs(context, attrs);
    setOnTouchListener(this);
  }

  public FlatToggleImageButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, android.R.attr.buttonStyle);

    mImvImage = new ImageView(context, attrs, defStyle);
    mImvImage.setId(generateViewId());
    mTvText = new TextView(context, attrs, defStyle);
    mTvText.setId(generateViewId());

    setOrientation(VERTICAL);

    LayoutParams params = new LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT,
        Gravity.CENTER);

    addView(mImvImage, params);

    LayoutParams tvParams = new LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT,
        Gravity.CENTER);

    addView(mTvText, tvParams);

    if (mImvImage.getDrawable() == null) {
      mImvImage.setVisibility(GONE);
    } else {
      mImvImage.setVisibility(VISIBLE);
    }

    if (mTvText.getText() == null) {
      mTvText.setVisibility(GONE);
    } else {
      mTvText.setVisibility(VISIBLE);
      if (mImvImage.getDrawable() == null) {
        mTvText.setGravity(Gravity.CENTER_VERTICAL);
      }
    }

    init();
    parseAttrs(context, attrs);
    setOnTouchListener(this);
  }

  private void init() {
    mIsShadowEnabled = true;
    Resources res = getResources();
    if (res == null) return;
    mButtonColor = res.getColor(R.color.fbutton_default_color);
    mShadowColor = res.getColor(R.color.fbutton_default_shadow_color);
    mShadowHeight = res.getDimensionPixelSize(R.dimen.fbutton_default_shadow_height);
    mCornerRadius = res.getDimensionPixelSize(R.dimen.fbutton_default_conner_radius);

    // this remove the shadow from button in android recent version.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setStateListAnimator(null);
    }
  }

  public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
    this.mListener = listener;
  }

  @Override
  public void setChecked(boolean checked) {
    setSelected(checked);
    if (mListener != null) {
      mListener.onCheckedChanged(this, checked);
    }

    if (checked) {
      mButtonColor = mCheckedColor;
      mTvText.setTextColor(mCheckedTextColor);
    } else {
      mButtonColor = mUncheckedColor;
      mTvText.setTextColor(mUncheckTextColor);
    }
    refresh();
  }

  @Override public boolean isChecked() {
    return isSelected();
  }

  @Override public void toggle() {
    setChecked(!isChecked());
  }

  @Override
  public boolean performClick() {
    toggle();
    return super.performClick();
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    switch (motionEvent.getAction()) {
      case MotionEvent.ACTION_DOWN:
        updateBackground(mPressedDrawable);
        setPadding(mPaddingLeft, mPaddingTop + 2 * mShadowHeight, mPaddingRight, mPaddingBottom);
        break;
      case MotionEvent.ACTION_MOVE:
        Rect r = new Rect();
        view.getLocalVisibleRect(r);
        if (!r.contains((int) motionEvent.getX(), (int) motionEvent.getY() + 3 * mShadowHeight) &&
            !r.contains((int) motionEvent.getX(), (int) motionEvent.getY() - 3 * mShadowHeight)) {
          updateBackground(mUnpressedDrawable);
          setPadding(mPaddingLeft, mPaddingTop + 2 * mShadowHeight, mPaddingRight,
              mPaddingBottom + mShadowHeight);
        }
        break;
      case MotionEvent.ACTION_OUTSIDE:
      case MotionEvent.ACTION_CANCEL:
      case MotionEvent.ACTION_UP:
        updateBackground(mUnpressedDrawable);
        setPadding(mPaddingLeft, mPaddingTop + mShadowHeight,
            mPaddingRight, mPaddingBottom + mShadowHeight);
        break;
    }
    return false;
  }

  private void parseAttrs(Context context, AttributeSet attrs) {
    TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.FlatToggleImageButton);

    mIsShadowEnabled = arr.getBoolean(R.styleable.FlatToggleImageButton_ftb_shadowEnabled, true);

    Resources res = context.getResources();
    int defaultColor = res.getColor(R.color.fbutton_default_color);
    mButtonColor = arr.getColor(R.styleable.FlatToggleImageButton_ftb_buttonColor, defaultColor);

    int defaultShadowColor = res.getColor(R.color.fbutton_default_shadow_color);
    mShadowColor =
        arr.getColor(R.styleable.FlatToggleImageButton_ftb_shadowColor, defaultShadowColor);
    mIsShadowColorDefined = mShadowColor != defaultShadowColor;

    mShadowHeight =
        arr.getDimensionPixelSize(R.styleable.FlatToggleImageButton_ftb_shadowHeight, 14);
    mCornerRadius =
        arr.getDimensionPixelSize(R.styleable.FlatToggleImageButton_ftb_cornerRadius, 8);

    mCheckedColor = arr.getColor(R.styleable.FlatToggleImageButton_ftb_checkedColor, defaultColor);

    int defaultUncheckedColor = res.getColor(R.color.fbutton_color_clouds);
    mUncheckedColor =
        arr.getColor(R.styleable.FlatToggleImageButton_ftb_uncheckedColor, defaultUncheckedColor);

    mCheckedTextColor =
        arr.getColor(R.styleable.FlatToggleImageButton_ftb_checkedTextColor, Color.WHITE);
    mUncheckTextColor =
        arr.getColor(R.styleable.FlatToggleImageButton_ftb_uncheckedTextColor, Color.GRAY);

    boolean checked = arr.getBoolean(R.styleable.FlatToggleImageButton_android_checked, false);
    // set initial state of the button.
    setSelected(checked);
    if (checked) {
      mButtonColor = mCheckedColor;
      mTvText.setTextColor(mCheckedTextColor);
    } else {
      mButtonColor = mUncheckedColor;
      mTvText.setTextColor(mUncheckTextColor);
    }

    arr.recycle();

    //Get paddingLeft, paddingRight
    int[] attrsArray = new int[] {
        android.R.attr.paddingLeft,  // 0
        android.R.attr.paddingRight, // 1
    };
    TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
    if (ta == null) return;
    mPaddingLeft = ta.getDimensionPixelSize(0, 0);
    mPaddingRight = ta.getDimensionPixelSize(1, 0);
    ta.recycle();

    //Get paddingTop, paddingBottom
    int[] attrsArray2 = new int[] {
        android.R.attr.paddingTop,   // 0
        android.R.attr.paddingBottom,// 1
    };
    TypedArray ta1 = context.obtainStyledAttributes(attrs, attrsArray2);
    if (ta1 == null) return;
    mPaddingTop = ta1.getDimensionPixelSize(0, 0);
    mPaddingBottom = ta1.getDimensionPixelSize(1, 0);
    ta1.recycle();

    LayoutParams params = (LayoutParams) mTvText.getLayoutParams();
    params.setMargins(0, -dpToPixel(mPaddingBottom + 2 * mShadowHeight), 0, 0);
    mTvText.setLayoutParams(params);

    refresh();
  }

  public void refresh() {
    int alpha = Color.alpha(mButtonColor);
    float[] hsv = new float[3];
    Color.colorToHSV(mButtonColor, hsv);
    hsv[2] *= 0.8f; // value component
    //if shadow color was not defined, generate shadow color = 80% brightness
    if (!mIsShadowColorDefined) mShadowColor = Color.HSVToColor(alpha, hsv);

    //Create pressed background and unpressed background drawables
    if (isEnabled()) {
      if (mIsShadowEnabled) {
        mPressedDrawable = createDrawable(mCornerRadius, Color.TRANSPARENT, mButtonColor);
        mUnpressedDrawable = createDrawable(mCornerRadius, mButtonColor, mShadowColor);
      } else {
        mShadowHeight = 0;
        mPressedDrawable = createDrawable(mCornerRadius, mShadowColor, Color.TRANSPARENT);
        mUnpressedDrawable = createDrawable(mCornerRadius, mButtonColor, Color.TRANSPARENT);
      }
    } else {
      Color.colorToHSV(mButtonColor, hsv);
      hsv[1] *= 0.25f; // saturation component
      int disabledColor = mShadowColor = Color.HSVToColor(alpha, hsv);
      // Disabled button does not have shadow
      mPressedDrawable = createDrawable(mCornerRadius, disabledColor, Color.TRANSPARENT);
      mUnpressedDrawable = createDrawable(mCornerRadius, disabledColor, Color.TRANSPARENT);
    }
    updateBackground(mUnpressedDrawable);
    setPadding(mPaddingLeft, mPaddingTop + mShadowHeight, mPaddingRight,
        mPaddingBottom + mShadowHeight);
  }

  /**
   * Set button background
   *
   * @param background background for button
   */
  private void updateBackground(Drawable background) {
    if (background == null) return;
    if (Build.VERSION.SDK_INT >= 16) {
      setBackground(background);
    } else {
      setBackgroundDrawable(background);
    }
  }

  private LayerDrawable createDrawable(int radius, int topColor, int bottomColor) {
    float[] outerRadius =
        new float[] { radius, radius, radius, radius, radius, radius, radius, radius };

    //Top
    RoundRectShape topRoundRect = new RoundRectShape(outerRadius, null, null);
    ShapeDrawable topShapeDrawable = new ShapeDrawable(topRoundRect);
    topShapeDrawable.getPaint().setColor(topColor);
    //Bottom
    RoundRectShape roundRectShape = new RoundRectShape(outerRadius, null, null);
    ShapeDrawable bottomShapeDrawable = new ShapeDrawable(roundRectShape);
    bottomShapeDrawable.getPaint().setColor(bottomColor);
    //Create array
    Drawable[] drawArray = { bottomShapeDrawable, topShapeDrawable };
    LayerDrawable layerDrawable = new LayerDrawable(drawArray);

    //Set shadow height
    if (mIsShadowEnabled && topColor != Color.TRANSPARENT) {
      //unpressed drawable
      layerDrawable.setLayerInset(0, 0, 0, 0, 0);
    } else {
      //pressed drawable
      layerDrawable.setLayerInset(0, 0, mShadowHeight, 0, 0);
    }
    layerDrawable.setLayerInset(1, 0, 0, 0, mShadowHeight);
    return layerDrawable;
  }


  public void setText(CharSequence text) {
    mTvText.setText(text);
  }

  public void setText(int resId) {
    mTvText.setText(resId);
  }

  public void setShadowEnabled(boolean isShadowEnabled) {
    this.mIsShadowEnabled = isShadowEnabled;
    setShadowHeight(0);
    refresh();
  }

  public void setButtonColor(int buttonColor) {
    mButtonColor = buttonColor;
    refresh();
  }

  public void setShadowColor(int shadowColor) {
    mShadowColor = shadowColor;
    mIsShadowColorDefined = true;
    refresh();
  }

  public void setShadowHeight(int shadowHeight) {
    mShadowHeight = shadowHeight;
    refresh();
  }

  public void setCornerRadius(int cornerRadius) {
    mCornerRadius = cornerRadius;
    refresh();
  }

  public void setButtonPadding(int left, int top, int right, int bottom) {
    mPaddingLeft = left;
    mPaddingRight = right;
    mPaddingTop = top;
    mPaddingBottom = bottom;
    refresh();
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    refresh();
  }

  public int getShadowColor() {
    return mShadowColor;
  }

  public boolean isShadowEnabled() {
    return mIsShadowEnabled;
  }

  public int getButtonColor() {
    return mButtonColor;
  }

  public int getShadowHeight() {
    return mShadowHeight;
  }

  public int getCornerRadius() {
    return mCornerRadius;
  }

  public int getCheckedColor() {
    return mCheckedColor;
  }

  public void setCheckedColor(int checkedColor) {
    this.mCheckedColor = checkedColor;
  }

  public int getUncheckedColor() {
    return mUncheckedColor;
  }

  public void setUncheckedColor(int uncheckedColor) {
    this.mUncheckedColor = uncheckedColor;
  }

  public int getCheckedTextColor() {
    return mCheckedTextColor;
  }

  public void setCheckedTextColor(int checkedTextColor) {
    this.mCheckedTextColor = checkedTextColor;
  }

  public int getUncheckTextColor() {
    return mUncheckTextColor;
  }

  public void setUncheckTextColor(int uncheckTextColor) {
    this.mUncheckTextColor = uncheckTextColor;
  }

  @Override
  public Parcelable onSaveInstanceState() {
    Parcelable superState = super.onSaveInstanceState();
    SavedState ss = new SavedState(superState);
    ss.checked = isChecked();
    return ss;
  }

  @Override
  public void onRestoreInstanceState(Parcelable state) {
    if (!(state instanceof SavedState)) {
      super.onRestoreInstanceState(state);
      return;
    }

    SavedState savedState = (SavedState) state;
    super.onRestoreInstanceState(savedState.getSuperState());
    OnCheckedChangeListener tempListener = mListener;
    mListener = null;
    setChecked(savedState.checked);
    mListener = tempListener;
  }

  static class SavedState extends BaseSavedState {
    boolean checked;

    SavedState(Parcelable state) {
      super(state);
    }

    private SavedState(Parcel parcel) {
      super(parcel);
      checked = parcel.readInt() == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      super.writeToParcel(dest, flags);
      dest.writeInt(checked ? 1 : 0);
    }

    @Override
    public int describeContents() {
      return 0;
    }

    //required field that makes Parcelables from a Parcel
    public static final Parcelable.Creator<SavedState> CREATOR =
        new Parcelable.Creator<SavedState>() {
          public SavedState createFromParcel(Parcel source) {
            return new SavedState(source);
          }

          public SavedState[] newArray(int size) {
            return new SavedState[size];
          }
        };
  }

  private int dpToPixel(int dp) {
    Resources r = getContext().getResources();
    return (int) TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        r.getDisplayMetrics()
    );
  }

  private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

  public static int generateViewId() {
    if (Build.VERSION.SDK_INT >= 17) {
      return View.generateViewId();
    } else {
      int result;
      int newValue;
      do {
        result = sNextGeneratedId.get();
        newValue = result + 1;
        if (newValue > 0x00FFFFFF) {
          newValue = 1;
        }
      } while (!sNextGeneratedId.compareAndSet(result, newValue));

      return result;
    }
  }
}
