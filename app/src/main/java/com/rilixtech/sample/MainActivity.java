package com.rilixtech.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.rilixtech.widget.flattoggleimagebutton.FlatToggleImageButton;

public class MainActivity extends AppCompatActivity {

  private FlatToggleImageButton mFtbSample;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mFtbSample = findViewById(R.id.main_ftb);
    mFtbSample.setOnCheckedChangeListener(new FlatToggleImageButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(FlatToggleImageButton button, boolean isChecked) {
        Toast.makeText(MainActivity.this, "isChecked = " + isChecked, Toast.LENGTH_SHORT).show();
      }
    });

    mFtbSample.setText("Testing");
  }
}
